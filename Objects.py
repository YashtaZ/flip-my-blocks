#!/usr/bin/env python3
"""Kacper Wenta"""
import pygame
import sys
import os


"""Class describing Rectangles and actions correlated to them """


class Rectangles:
    def __init__(self, screen, x, y, text, font):
        self.X = x
        self.Y = y
        self.width = 125
        self.height = 40
        self.screen = screen
        self.color = (64, 64, 64)
        self.font = font
        self.text = font.render(text, True, (255, 255, 255))

    """Method displaying rectangles and text"""
    def draw(self):
        pygame.draw.rect(self.screen, self.color, pygame.Rect(self.X, self.Y, self.width, self.height))
        self.screen.blit(self.text, (self.X+5, self.Y))

    """Method made to display the header"""
    def draw_text(self):
        self.screen.blit(self.text, (self.X + 5, self.Y))

    """Checking if the mouse hovers over given pos"""
    def is_over(self, pos):
        # Pos is the mouse position or a tuple of (x,y) coordinates
        if self.X < pos[0] < self.X + self.width:
            if self.Y < pos[1] < self.Y + self.height:
                return True

        return False

    """Action reader"""
    def action(self, event, mouse, rect, identifier):

        if event.type == pygame.MOUSEBUTTONDOWN:
            if rect.is_over(mouse):
                if identifier == 4:
                    return 1
                elif identifier == 1:
                    return 2
                elif identifier == 2:
                    return 3
                else:
                    return 4
        if event.type == pygame.MOUSEMOTION:
            if rect.is_over(mouse):
                rect.color = (0, 0, 0)
            else:
                rect.color = (64, 64, 64)


"""Class describing mechanics of the main game"""
class Game:

    """Methods returning x and y position of basically every block this part of program runs in loop so
    it is continuously checking whether mouse is over any block
    """

    def get_x(self, kiki):

        kaka = str(kiki)
        chars = []
        for k in kaka:
            chars.append(k)
        X = int(chars[6] + chars[7] + chars[8])
        return X

    def get_y(self, kiki):

        kaka = str(kiki)
        chars = []
        for k in kaka:
            chars.append(k)
        Y = int(chars[11] + chars[12] + chars[13])
        return Y

    def color_change(self, rect, color):
        temp = str(rect)

    """Same method as mentioned above but this time it gets values in a different way"""
    def is_over(self, pos, kiki):

        X = Game.get_x(self, kiki)
        Y = Game.get_y(self, kiki)

        # Pos is the mouse position or a tuple of (x,y) coordinates
        if X < pos[0] < X + 80:
            if Y < pos[1] < Y + 80:
                return True
        return False

    def game_action(self, event, mouse, var_file):
        """If Mouse is above any of the blocks return 1 in other cases return 2"""
        obj = Game()

        if event.type == pygame.MOUSEBUTTONUP and obj.is_over(mouse, var_file):
            return str(var_file)







