#!/usr/bin/env python3
"""Kacper Wenta"""
from random import randrange

import pygame
import sys
import os
import Objects

"""Centering window position and initializing pygame"""
os.environ['SDL_VIDEO_CENTERED'] = '1'
pygame.init()

"""Creating the first display and setting tittle & bgc"""
screen = pygame.display.set_mode((600, 500))
pygame.display.set_caption("Flipping blocks")
screen.fill((128, 128, 128))

"""Adding fonts to the program"""
font = pygame.font.SysFont('arial', 24, 0, 0)
title = pygame.font.SysFont('arial', 60, 1, 0)

"""Creating Menu objects"""
rect0 = Objects.Rectangles(screen, 125, 0, "Flip my blocks", title)
rect1 = Objects.Rectangles(screen, 230, 100, "New Game", font)
rect2 = Objects.Rectangles(screen, 230, 150, "Rank", font)
rect3 = Objects.Rectangles(screen, 230, 200, "About", font)
rect4 = Objects.Rectangles(screen, 230, 250, "Quit", font)
rect5 = Objects.Rectangles(screen, 230, 230, "U have won", font)

"""Function responsible for action assignment"""
def actions_menu():
    a = rect1.action(event, mouse, rect1, 1)
    b = rect2.action(event, mouse, rect2, 2)
    c = rect3.action(event, mouse, rect3, 3)
    d = rect4.action(event, mouse, rect4, 4)
    if a is not None:
        return a
    elif b is not None:
        return b
    elif c is not None:
        return c
    elif d is not None:
        return d


"""Function that draws menu"""
def drawing_menu():
    rect0.draw_text()
    rect1.draw()
    rect2.draw()
    rect3.draw()
    rect4.draw()


offset_x = 125
offset_y = 100
st_x = offset_x
nd_x = 90 + offset_x
rd_x = 180 + offset_x
th_x = 270 + offset_x
st_y = offset_y
nd_y = 90 + offset_y
rd_y = 180 + offset_y
th_y = 270 + offset_y
width = 80
height = 80

"""List containing all the rectangles """

rect_list = [
    pygame.Rect(st_x, st_y, width, height),
    pygame.Rect(nd_x, st_y, width, height),
    pygame.Rect(rd_x, st_y, width, height),
    pygame.Rect(th_x, st_y, width, height),
    pygame.Rect(st_x, nd_y, width, height),
    pygame.Rect(nd_x, nd_y, width, height),
    pygame.Rect(rd_x, nd_y, width, height),
    pygame.Rect(th_x, nd_y, width, height),
    pygame.Rect(st_x, rd_y, width, height),
    pygame.Rect(nd_x, rd_y, width, height),
    pygame.Rect(rd_x, rd_y, width, height),
    pygame.Rect(th_x, rd_y, width, height),
    pygame.Rect(st_x, th_y, width, height),
    pygame.Rect(nd_x, th_y, width, height),
    pygame.Rect(rd_x, th_y, width, height),
    pygame.Rect(th_x, th_y, width, height),
]
obj = Objects.Game()

color_list = [
    [(192, 192, 192), 0],
    [(51, 102, 0), 0],
    [(0, 0, 255), 0],
    [(123, 123, 123), 0],
    [(10, 10, 10), 0],
    [(75, 32, 123), 0],
    [(101, 1, 67), 0],
    [(255, 83, 21), 0]
]
"""
@:param colors_db = {rect:color}
"""
colors_db = {}
"""Function that assigns color to given rectangle if the rectangle already exists in db it takes value from there"""


def assign_color(rect):
    if str(rect) in colors_db:
        exit_color = colors_db[str(rect)]
    else:
        index = randrange(0, 7)
        color = color_list[index][0]
        if color_list[index][1] > 1:
            for x in range(0, len(color_list), 1):
                if color_list[x][1] == 0:
                    color = color_list[x][0]
                    color_list[x][1] += 1
                    break
                elif color_list[x][1] == 1:
                    color = color_list[x][0]
                    color_list[x][1] += 1
                    break

            colors_db[str(rect)] = color
            exit_color = color
        else:
            colors_db[str(rect)] = color
            color_list[index][1] += 1
            exit_color = color_list[index][0]

    return exit_color


"""Function made to get mouse position after first click"""

def get_cord_a(kord, kord_b):
    for x in range(0, len(rect_list), 1):
        if obj.game_action(event, mouse, rect_list[x]) is not None:
            if obj.game_action(event, mouse, rect_list[x]) != kord:
                if kord_b != kord:
                    kord = obj.game_action(event, mouse, rect_list[x])

    return kord


"""Function made to get mouse position after second click
@:param kord_a holds the value of mouse pos after 1st click
@:param kord_b holds the value of mouse pos after 2nd click
"""

def get_cord_b(kord_a ,kord_b):
    for x in range(0, len(rect_list), 1):
        if obj.game_action(event, mouse, rect_list[x]) is not None:
            if obj.game_action(event, mouse, rect_list[x]) != kord_b:
                bufor = obj.game_action(event, mouse, rect_list[x])
                if bufor != kord_a:
                    kord_b = obj.game_action(event, mouse, rect_list[x])
    return kord_b


"""Function gets cords as above then using assign_color function draws clicked rectangles on the screen"""

def draw_game(cord_a, cord_b, values_before):
    color_a = values_before[0]
    color_b = values_before[1]
    rect_a = values_before[2]
    rect_b = values_before[3]
    for x in range(0, len(rect_list), 1):

        if str(rect_list[x]) == cord_a:
            color_a = assign_color(rect_list[x])
            pygame.draw.rect(screen, color_a, rect_list[x])
            rect_a = rect_list[x]

        elif str(rect_list[x]) == cord_b:
            color_b = assign_color(rect_list[x])
            pygame.draw.rect(screen, color_b, rect_list[x])
            rect_b = rect_list[x]

        else:
            pygame.draw.rect(screen, (64, 64, 64), rect_list[x])
    pygame.display.flip()
    values = [color_a, color_b, rect_a, rect_b]
    return values


"""Function check if both of the clicked rectangles have the same color if so it changes theirs color to bg color
    if not it assigns them a basic color as if they were not clicked at all
    @:param wabz_list contains values as:
    @:param result stores 0 or 1. returns 0 if no changes were made and 1 if changes occur
    @:param change changes to 1 if any of given if's went through positive
    @:param rect_a, rect_b coordinates of rectangles that were manipulated
"""


def understanding_colors(taken_values):

    color_a = taken_values[0]
    color_b = taken_values[1]
    rect_a = taken_values[2]
    rect_b = taken_values[3]
    change = 0
    result = ""
    if color_a == color_b and color_a != "":
        pygame.draw.rect(screen, (0, 0, 0), rect_a)
        pygame.draw.rect(screen, (0, 0, 0), rect_b)
        result = 1
        change = 1

    elif color_a != color_b and color_a != "" and color_b != "":
        pygame.draw.rect(screen, (64, 64, 64), rect_a)
        pygame.draw.rect(screen, (64, 64, 64), rect_b)
        result = 0
        change = 1
    wabz_list = [result, rect_a, rect_b, change]
    return wabz_list


def rank_open_file():
    file = open("rank.txt", "r")
    linie = file.readlines()
    linie.sort()
    file.close()
    return linie


def save_time(time):
    file_save = open("rank.txt", "a")
    file_save.write(str(time)+"\n")


"""
@:param kord_a initializing params
@:param kord_b initializing params
@:param launch param needed for menu it changes its value depending on what user have clicked in the menu
@:param condition changes its value when kord_a and kord_b are not empty
@:param y, z temp values needed for popping values out of rectlist
"""
launch = 0
kord_a = ""
kord_b = " "
condition = 0
y = 5
z = 5
change = 0
clock = pygame.time.Clock()
values_before = ["", "", "", ""]
czas = 0
while True:
    # Event handling
    if len(rect_list) != 0:
        czas += clock.tick()
    mouse = pygame.mouse.get_pos()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit(0)
    if launch != 1 and launch != 2:
        var = actions_menu()
        drawing_menu()
        if var == 1:
            sys.exit(0)
        elif var == 2:
            pygame.time.delay(500)
            launch = 1
        elif var == 3:
            screen.fill((128, 128, 128))
            file = rank_open_file()
            for x in range(0, len(file), 1):
                text = Objects.Rectangles(screen, 200, 0+(x*20), (str(x+1)+" Place: "+str(file[x])+" s"), font)
                text.draw_text()

            pygame.display.flip()
            pygame.time.delay(3000)
            launch = 0
            screen.fill((128, 128, 128))
            pygame.display.flip()
        elif var == 4:
            screen.fill((128, 128, 128))
            about = Objects.Rectangles(screen, 200, 50, "Flip my blocks", font)
            about_one = Objects.Rectangles(screen, 200, 75, "Version 1.0", font)
            about_two = Objects.Rectangles(screen, 200, 100, "Game written by ", font)
            about_three = Objects.Rectangles(screen, 200, 125, "Kacper Wenta", font)

            about.draw_text()
            about_one.draw_text()
            about_two.draw_text()
            about_three.draw_text()

            pygame.display.flip()
            pygame.time.delay(3000)

            launch = 0
            screen.fill((128, 128, 128))
            pygame.display.flip()

    if launch == 1:
        screen.fill((128, 128, 128))

        if condition != 2:
            if condition == 0:
                if kord_a == "":
                    kord_a = get_cord_a(kord_a, kord_b)
                elif kord_a != "":
                    print(kord_a)
                    condition = condition + 1

            if condition == 1:
                if kord_b == " ":
                    kord_b = get_cord_b(kord_a, kord_b)
                elif kord_b != "":
                    print(kord_b)
                    condition = condition + 1

        values = draw_game(kord_a, kord_b, values_before)
        if values[2] != "" and values[3] != "":
            zwrot = understanding_colors(values)
            if zwrot[3] == 1:

                if zwrot[0] == 1:
                    y = rect_list.index(zwrot[2])
                    z = rect_list.index(zwrot[1])
                    if rect_list[y] is not None and rect_list[z] is not None:
                        rect_list.pop(y)
                        if y > z:
                            rect_list.pop(z)
                        else:
                            rect_list.pop(z - 1)
                    kord_a = ""
                    kord_b = " "
                    condition = 0

                elif zwrot[0] == 0:
                    kord_a = ""
                    kord_b = " "
                    condition = 0

    if len(rect_list) == 0:
        screen.fill((128, 128, 128))
        rect5.draw_text()
        pygame.display.update()
        save_time(czas/1000)
        pygame.time.delay(1000)
        sys.exit(0)

    pygame.display.update()

